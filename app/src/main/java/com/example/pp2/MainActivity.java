package com.example.pp2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void oblicz(View view) {

        EditText editText = (EditText) findViewById(R.id.editText1);
        double liczba = Double.parseDouble(String.valueOf(editText.getText()));
        TextView textView = (TextView) findViewById(R.id.wynik);
        RadioGroup RG =  (RadioGroup) findViewById(R.id.rG);
        RadioButton cm =  (RadioButton) findViewById(R.id.cm);
        RadioButton km =  (RadioButton) findViewById(R.id.km);
        RadioButton mile = (RadioButton) findViewById(R.id.mile);
        RadioButton mm =  (RadioButton) findViewById(R.id.mm);
        String wyn;
            int selectId = RG.getCheckedRadioButtonId();

        if(selectId==cm.getId()){
            liczba = liczba * 100;
            wyn = "" + liczba;
            textView.setText(wyn);
        }else if(selectId==km.getId()){
            liczba = liczba / 1000;
            wyn = "" + liczba;
            textView.setText(wyn);
        }else if(selectId==mile.getId()){
            liczba = liczba / 1500;
            wyn = "" + liczba;
            textView.setText(wyn);
        }else if(selectId==mm.getId()){
            liczba = liczba * 1000;
            wyn = "" + liczba;
            textView.setText(wyn);
        }else{
            textView.setText("Error: Wybierz przycisk");
        }




    }
}
